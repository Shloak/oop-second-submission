//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<conio.h>
#include<string.h>
#include<stdlib.h>

using namespace std;

//***************************************************************
//         GLOBAL VARIABLES
//****************************************************************

int cu=0, pr=0;
void main_menu();											//FUNCTION DECLARATION

//***************************************************************
//    	   CLASS FOR CUSTOMER
//****************************************************************

class Customer
{   char name[20];
	long int no;

    public:

        void get_Cus();
        void put_Cus();

    Customer()                                                  //CONSTRUCTOR
    {   no=0;
        strcpy(name,"NO NAME");
    }

    long int ret_No()
    {	return no;	}

    char* ret_Name()
  	{	return name;	}

}cus[10];

void Customer::get_Cus()                                        //DECLARING FUNCTION get_Cus
{   cout<<"\n\t Enter Customer Name:";
    cin>>name;

    cout<<"\n\t Enter Customer Phone Number:";
    cin>>no;
}

void Customer::put_Cus()                                        //DECLARING FUNCTION put_Cus
{   cout<<"\n\t Customer Name:"<<name;
    cout<<"\n\t Customer Phone Number:"<<no;
    getch();
}

//***************************************************************
//    	   CLASS FOR PRODUCT
//****************************************************************

class Product
{   int price, prono;
    char proname[20];

    public:

        void get_Pro();
        void put_Pro();

    Product()                                                   //CONSTRUCTOR
    {   prono=0;
        price=0;
        strcpy(proname,"NO NAME");
    }

    int ret_Prono()
    {	return prono;	}

    int ret_Price()
    {	return price;	}

    char* ret_Proname()
  	{	return proname;	}

}pro[10];

 void Product::get_Pro()                                        //DECLARING FUNCTION get_Pro
{   cout<<"\n\t Enter Product Number:";
    cin>>prono;

    cout<<"\n\t Enter Product Name:";
    cin>>proname;

    cout<<"\n\t Enter Product Price:";
    cin>>price;
}

void Product::put_Pro()                                         //DECLARING FUNCTION put_Pro
{   cout<<"\n\t Product Name:"<<proname;
    cout<<"\n\t Product Number:"<<prono;
    cout<<"\n\t Product Price:"<<price;
    getch();
}

//***************************************************************
//    	FUNCTION FOR CREATING A NEW CUSTOMER ACCOUNT
//****************************************************************

void add_cus()
{   system("cls");
    char ch;
    do{
        cus[cu].get_Cus();
        cu++;
        cout<<"\nDo You Want To Add Another Customer ? (y/n)";
        cin>>ch;
        cout<<"\n";
	}while( ch == 'y' || ch == 'Y' );
}


//***************************************************************
//    	FUNCTION OF CUSTOMER DETAIL
//****************************************************************

void cus_detail()
{   int i,flag=0;
    long int ph;
    char ch;
    do
	{   system("cls");
		cout<<"\n\n\n\tMENU";
		if(cu==0)
            cout<<"\n\n\n\n************  PLEASE ADD CUSTOMER FIRST  ************\n\n\n\n";
		cout<<"\n\n\t1. ADD CUSTOMER";
		cout<<"\n\n\t2. SEARCH CUSTOMER";
		cout<<"\n\n\t3. RETURN TO MAIN MENU";
		cout<<"\n\n\tPlease Select Your Option (1-3) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
                add_cus();
				break;
			case '2':
			    system("cls");
			    cout<<"\n\tEnter Customer Phone Number: ";
                cin>>ph;
                for(i=0;i<cu;i++)
                   if(cus[i].ret_No()==ph)
                        {   system("cls");
                            cus[i].put_Cus();
                            flag = 1;
                        }
                if(!flag)
                {    cout<<"\n\tSORRY NO SUCH CUSTOMER EXIST";
                     getch();
                }
				break;
			case '3':
               main_menu();
           default:
			    cus_detail();
		}
	}while(ch!='3');

}

//***************************************************************
//    	FUNCTION FOR CREATING A NEW PRODUCT
//****************************************************************

void add_pro()
{   system("cls");
    char ch;
    do{
        pro[pr].get_Pro();
        pr++;
        cout<<"\nDo You Want To Add Another Product ? (y/n)";
        cin>>ch;
      cout<<"\n";
	}while( ch == 'y' || ch == 'Y' );
}

//***************************************************************
//    	FUNCTION FOR SHOWING ALL PRODUCTS
//****************************************************************

void show_pro()
{   system("cls");
    int i;
    cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\n\n Sr No.\t Pr No.\t\t Pr Name\t\tPrice\n";
	cout<<"\n--------------------------------------------------------------------------------";
	for(i=0;i<pr;i++)
        cout<<"\n "<<(i+1)<<"\t "<<pro[i].ret_Prono()<<"\t\t "<<pro[i].ret_Proname()<<"\t\t "<<pro[i].ret_Price();
    cout<<"\n\n\t\tenter to continue";
    getch();
}

//***************************************************************
//    	FUNCTION OF PRODUCT DETAIL
//****************************************************************

void pro_detail()
{   int no,i,flag=0;
    char ch;
    do
	{   system("cls");
		cout<<"\n\n\n\tMENU";
		if(pr==0)
            cout<<"\n\n\n\n************  PLEASE ADD PRODUCT FIRST  ************\n\n\n\n";
		cout<<"\n\n\t1. ADD PRODUCT";
		cout<<"\n\n\t2. SEARCH PRODUCT";
		cout<<"\n\n\t3. SHOW ALL PRODUCTS";
		cout<<"\n\n\t4. RETURN TO MAIN MENU";
		cout<<"\n\n\tPlease Select Your Option (1-4) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
                add_pro();
				break;
			case '2':
			    system("cls");
			    cout<<"\n\tEnter Product Number: ";
                cin>>no;
                for(i=0;i<pr;i++)
                   if(pro[i].ret_Prono()==no)
                        {   system("cls");
                            pro[i].put_Pro();
                            flag =1;
                        }
                if(!flag)
                {    cout<<"\n\tSORRY NO SUCH PRODUCT EXIST";
                     getch();
                }
				break;
			case '3':
				show_pro();
				break;
            case '4':
               main_menu();
            default:
			    cus_detail();
		}
	}while(ch!='4');

}

//***************************************************************
//  FUNCTION TO PLACE ORDER AND GENERATING BILL FOR PRODUCTS
//****************************************************************

void bill()
{   system("cls");
    int  total=0,qty=0,amt,order_arr[50],quan[50],c=0,i,x;
	char ch;
	show_pro();
	cout<<"\n................................................................................";
	cout<<"\n\t\t\t\tPLACE YOUR ORDER";
	cout<<"\n................................................................................\n";
	do{     cout<<"\n\nEnter The Product No. Of The Product : ";
            cin>>order_arr[c];
            cout<<"\nQuantity in number : ";
            cin>>quan[c];
            c++;
            cout<<"\nDo You Want To Order Another Product ? (y/n)";
            ch = getche();
            cout<<endl;
	}while( ch == 'y' || ch == 'Y' );
	cout<<"\n\nThank You For Placing The Order";
	getch();
	system("cls");
	cout<<"\n\t\t\t  GLORIOUS ELECTRONICS";
	cout<<"\n\t\t\t\tCASH MEMO";
	cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\n\n Sr No.\t Pr No.\t Pr Name\t\tQty \tPrice \t Amount\n";
	cout<<"\n--------------------------------------------------------------------------------";
	for(x=0;x<c;x++)
	{
	    for(i=0;i<pr;i++)
        {   if(pro[i].ret_Prono()==order_arr[x])
            {
				amt=(pro[i].ret_Price())*(quan[x]);
				cout<<"\n "<<(i+1)<<"\t "<<order_arr[x]<<"\t "<<pro[i].ret_Proname()<<"\t\t "<<quan[x]<<"\t "<<pro[i].ret_Price()<<"\t "<<amt;
				total+=amt;
				qty+=quan[x];
			 }

		}
	}
	cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\t\tQUANTITY = "<<qty<<"\t\tTOTAL = "<<total;
	cout<<"\n\n--------------------------------------------------------------------------------\n\n";
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

void main_menu()
{   char ch;
    do
	{   system("cls");
        cout<<"\n\n\n\tMAIN MENU";
		cout<<"\n\n\t1. CUSTOMER DETAILS";
		cout<<"\n\n\t2. PRODUCT DETAILS";
		cout<<"\n\n\t3. GENERATE BILL";
        cout<<"\n\n\t4. EXIT";
		cout<<"\n\n\tPlease Select Your Option (1-4) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
				cus_detail();
				main_menu();
			case '2':
			    pro_detail();
				main_menu();
			case '3':
			    bill();
			case '4':
                exit(0);
		}
	}while(ch!='4');
}

//***************************************************************
//    	INTRODUCTION FUNCTION
//****************************************************************

void intro()
{
    system("cls");
    cout<<"\n\t\t\t   GLORIOUS ELECTRONICS\n";
    cout<<"\n\n\t\t         MADE BY : SHLOAK AGARWAL";
    cout<<"\n\n\t\t             ROLL NO : 1401105";
    cout<<"\n\n\t\t             enter to continue";
    getch();
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{
    intro();
	main_menu();

	return 0;
}

//***************************************************************
//    			END OF PROGRAM
//***************************************************************
